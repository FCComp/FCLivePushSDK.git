
Pod::Spec.new do |s|

  s.name         = "FCLivePushSDK"
  s.version      = "1.0.9"
  s.summary      = "FCLivePushSDK"
  s.description  = <<-DESC
FCLivePushSDK 直播推流SDK
                   DESC
  s.homepage     = "https://gitee.com/FCComp/FCLivePushSDK.git"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  # s.license      = "MIT (example)"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "fczhouyou" => "zhouyou@sobey.com" }
  # Or just: s.author    = "ZhouYou"
  # s.authors            = { "ZhouYou" => "zhouyou@sobey.com" }
  # s.social_media_url   = "http://twitter.com/ZhouYou"
  # s.platform     = :ios
  s.platform     = :ios, "10.0"

  #  When using multiple platforms
  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"
  # s.tvos.deployment_target = "9.0"

  s.source       = { :git => "https://gitee.com/FCComp/FCLivePushSDK.git", :tag => "#{s.version}" }
  s.source_files  = "FCLivePushSDK/FCNewLiveSDK.framework/Headers/*.h"
  # s.exclude_files = "Classes/Exclude"
  # s.public_header_files = "Classes/**/*.h"
   s.resource  = "FCLivePushSDK/*.bundle"
  # s.resources = "Resources/*.png"
  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"
  s.vendored_frameworks = 'FCLivePushSDK/*.framework'
  # s.framework  = "SomeFramework"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  
s.dependency 'SDWebImage'
s.dependency 'Masonry'
s.dependency 'AFNetworking'
s.dependency 'pop'
s.dependency 'MJRefresh'
s.dependency 'MBProgressHUD'
s.dependency 'FDFullscreenPopGesture'
s.dependency 'AFNetworking'
s.dependency 'YYModel'
s.dependency 'DZNEmptyDataSet'
 
end
