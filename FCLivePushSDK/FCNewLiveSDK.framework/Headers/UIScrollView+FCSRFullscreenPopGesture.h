//
//  UIScrollView+FCSRFullscreenPopGesture.h
//  FCSRTools
//
//  Created by Summer on 2020/4/10.
//  Copyright © 2020 sobey. All rights reserved.
// 支持嵌套scrollView全屏手势返回

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (FCSRFullscreenPopGesture)

@end

NS_ASSUME_NONNULL_END
