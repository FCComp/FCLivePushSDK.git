//
//  FCNewLiveBaseHttp.h
//  FCNewLiveSDK
//
//  Created by Summer on 2019/12/26.
//  Copyright © 2019 Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FCNewLiveBaseHttpSSLConfig.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    FCNewLiveRequestGet,
    FCNewLiveRequestPost,
    FCNewLiveRequestDelete,
    FCNewLiveRequestPut,
} FCNewLiveRequestType;//请求类型

typedef enum : NSUInteger {
    FCNewLiveRequestSerializerTypeJson,
    FCNewLiveRequestSerializerTypeHttp,
} FCNewLiveRequestSerializerType;//requestSerializer

typedef enum : NSUInteger {
    FCNewLiveResponseSerializerTypeJson,
    FCNewLiveResponseSerializerTypeHttp,
    FCNewLiveResponseSerializerTypeXml,
} FCNewLiveResponseSerializerType;//responseSerializer

typedef void(^FCNewLiveRequestComplete)(id _Nullable data,  NSString * _Nullable error);

@interface FCNewLiveBaseHttp : NSObject

@property (nonatomic, copy) NSString *baseUrl;//请求域名，比如：http://FCNewLive.sobeylingyun.com

@property (nonatomic, strong, readonly) NSURLSessionTask *requestTask;

@property (nonatomic, copy, nullable) FCNewLiveRequestComplete resultBlock;//请求结果回调

@property (nonatomic, strong) id param;//请求参数

@property (nonatomic, assign) FCNewLiveRequestType requestType;//请求类型，get,post

@property (nonatomic, assign) NSTimeInterval requestTimeoutInterval;//超时时间

@property (nonatomic, assign) FCNewLiveRequestSerializerType requestSerializerType;//默认FCNewLiveRequestSerializerTypeJson

@property (nonatomic, assign) FCNewLiveResponseSerializerType responseSerializerType;//默认FCNewLiveResponseSerializerTypeJson

@property (nonatomic, copy) NSString *requestURLPath;//请求接口地址，比如/FCNewLive/api/auth/login

@property (nonatomic, strong) NSDictionary<NSString *, NSString *> * requestHeaderFieldValueDictionary;//header参数

@property (nonatomic, strong) FCNewLiveBaseHttpSSLConfig *sslConfig;//ssl配置

- (void)startRequest;//开始请求

+ (FCNewLiveBaseHttp *)instanceWithPostUrl:(NSString *)url
                             baseUrl:(NSString *)baseUrl
                               param:(nullable id)param
                              result:(FCNewLiveRequestComplete)resultBlock;

+ (FCNewLiveBaseHttp *)instanceWithGetUrl:(NSString *)url
                            baseUrl:(NSString *)baseUrl
                              param:(nullable id)param
                             result:(FCNewLiveRequestComplete)resultBlock;

+ (void)postWithUrl:(NSString *)url
            baseUrl:(NSString *)baseUrl
              param:(nullable id)param
             result:(FCNewLiveRequestComplete)resultBlock;

+ (void)getWithUrl:(NSString *)url
           baseUrl:(NSString *)baseUrl
             param:(nullable id)param
            result:(FCNewLiveRequestComplete)resultBlock;

+ (FCNewLiveBaseHttp *)instanceWithDeleteUrl:(NSString *)url baseUrl:(NSString *)baseUrl param:(id)param result:(FCNewLiveRequestComplete)resultBlock;

+ (void)deleteWithUrl:(NSString *)url baseUrl:(NSString *)baseUrl param:(id)param result:(FCNewLiveRequestComplete)resultBlock;

+ (FCNewLiveBaseHttp *)instanceWithPutUrl:(NSString *)url baseUrl:(NSString *)baseUrl param:(id)param result:(FCNewLiveRequestComplete)resultBlock;

+ (void)putWithUrl:(NSString *)url baseUrl:(NSString *)baseUrl param:(id)param result:(FCNewLiveRequestComplete)resultBlock;

@end

NS_ASSUME_NONNULL_END
