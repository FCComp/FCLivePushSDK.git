//
//  FCNewLiveTenantConfig.h
//  FCNewLiveSDK
//
//  Created by sobey on 2020/8/25.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FCNewLiveTenantUser.h"



@interface FCNewLiveTenantConfig : NSObject

/**
 @param domain 域名
 @param themeColor app主题色
 */
+ (void)configDomain:(NSString *)domain themeColor:(UIColor *)themeColor;

/**
 获取用户信息
 */
+ (void)configUserMobile:(NSString *)mobile access_key:(NSString *)access_key complete:(void(^)(BOOL success,NSDictionary *userInfo))block;
/**
 注册用户信息
 */
+ (void)configTenantInfo:(FCNewLiveTenantUser *)info;

@end

