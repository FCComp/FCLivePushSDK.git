//
//  FCSRFloatCompare.h
//  FCNewLiveProject
//
//  Created by Summer on 2020/1/15.
//  Copyright © 2020 sobey. All rights reserved.
// 浮点数判断

#ifndef FCSRFloatCompare_h
#define FCSRFloatCompare_h


#ifdef F_EQUAL
#undef F_EQUAL
#endif

#ifdef F_NOT_EQUAL
#undef F_NOT_EQUAL
#endif

#ifdef F_EQUAL_ZERO
#undef F_EQUAL_ZERO
#endif

#ifdef F_NOT_EQUAL_ZERO
#undef F_NOT_EQUAL_ZERO
#endif

#ifdef F_LESS_THAN
#undef F_LESS_THAN
#endif

#ifdef F_LESS_OR_EQUAL_THAN
#undef F_LESS_OR_EQUAL_THAN
#endif

#ifdef F_GREATER_THAN
#undef F_GREATER_THAN
#endif

#ifdef F_GREATER_OR_EQUAL_THAN
#undef F_GREATER_OR_EQUAL_THAN
#endif

#define F_EQUAL(a, b) ((fabs((a) - (b))) < FLT_EPSILON)

#define F_NOT_EQUAL(a, b) (!(F_EQUAL(a, b)))

#define F_EQUAL_ZERO(a) (fabs(a) < FLT_EPSILON)

#define F_NOT_EQUAL_ZERO(a) (!(F_EQUAL_ZERO(a)))

#define F_LESS_THAN(a, b) (((a) - (b)) < FLT_EPSILON && F_NOT_EQUAL(a, b))

#define F_LESS_OR_EQUAL_THAN(a, b) (F_LESS_THAN(a, b) || F_EQUAL(a, b))

#define F_GREATER_THAN(a, b) (((a) - (b)) > FLT_EPSILON && F_NOT_EQUAL(a, b))

#define F_GREATER_OR_EQUAL_THAN(a, b) (F_GREATER_THAN(a, b) || F_EQUAL(a, b))


#endif /* FCSRFloatCompare_h */
