//
//  FCNewLiveBaseHttpManager.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/1/8.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class FCNewLiveBaseHttp;

@interface FCNewLiveBaseHttpManagerModel : NSObject

@property (nonatomic, strong) NSString *vc;
@property (nonatomic, strong) FCNewLiveBaseHttp *request;

@end


@interface FCNewLiveBaseHttpManager : NSObject

@property (nonatomic, strong) NSMutableArray *requestModels;

+ (FCNewLiveBaseHttpManager *)instance;

- (void)addRequest:(FCNewLiveBaseHttp *)request;

- (void)removeRequest:(FCNewLiveBaseHttp *)request;

- (void)cancelAllRequestWithVc:(NSString *)vc;

@end

NS_ASSUME_NONNULL_END
