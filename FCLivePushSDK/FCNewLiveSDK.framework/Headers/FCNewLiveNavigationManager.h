//
//  FCNewLiveNavigationManager.h
//  FCNewLiveNavigation
//
//  Created by Summer on 2020/1/3.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCNewLiveNavigationManager : NSObject
@property (nonatomic, strong) id navController;
+ (instancetype)manager;
@end

NS_ASSUME_NONNULL_END
