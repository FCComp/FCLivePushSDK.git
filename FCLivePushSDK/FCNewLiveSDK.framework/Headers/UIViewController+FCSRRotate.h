//
//  UIViewController+FCSRRotate.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/7/2.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 设备旋转支持的方向，为了更好的进行位运算，所以未采用系统设备方向枚举

 - FCSRRotateOrientationPortrait: 竖屏
 - FCSRRotateOrientationLandscapeRight: 横屏，home键在右侧
 - FCSRRotateOrientationLandscapeLeft: 横屏，home键做左侧
 - FCSRRotateOrientationLandscape: 横屏，包括OMRotateOrientationLandscapeLeft和OMRotateOrientationLandscapeRight
 - FCSRRotateOrientationPortraitUpsideDown: 倒立竖屏
 - FCSRRotateOrientationAll: 所有状态，两种横屏和两种竖屏的状态
 - FCSRRotateOrientationAllButUpsideDown: 除了倒立竖屏的其他所有方向
 */
typedef NS_ENUM(NSUInteger, FCSRRotateOrientation) {
    FCSRRotateOrientationPortrait             = (1 << 0),
    FCSRRotateOrientationLandscapeRight        = (1 << 1),
    FCSRRotateOrientationLandscapeLeft       = (1 << 2),
    FCSRRotateOrientationLandscape            = (1 << 3),
    FCSRRotateOrientationPortraitUpsideDown   = (1 << 4),
    FCSRRotateOrientationAll                  = (1 << 5),
    FCSRRotateOrientationAllButUpsideDown     = (1 << 6),
};

@interface UIViewController (FCSRRotate)

/** 支持的方向，只有一个方向时即为强转。当有多个参数时，将以 竖屏-左转-右转-倒立竖屏 顺序来优先强转第一个方向*/
@property (assign, nonatomic) FCSRRotateOrientation fcsrSupportOrientations;

@end


@interface UINavigationController (FCSRRotate)

@end

@interface UITabBarController (FCSRRotate)

@end
