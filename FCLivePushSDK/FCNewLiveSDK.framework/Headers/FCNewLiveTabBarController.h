//
//  FCNewLiveTabBarController.h
//  FCNewLiveNavigation
//
//  Created by Summer on 2020/1/3.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCNewLiveTabBarController : UITabBarController

@property (nonatomic, strong) NSArray *items;

@end

NS_ASSUME_NONNULL_END
